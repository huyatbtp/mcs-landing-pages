import { Box } from '@chakra-ui/core'
const ContentWrapper = ({ children }) =>
  <Box maxW="1200px" mx="auto">
    {children}
  </Box>;
export default ContentWrapper