import { Icon, Text, Flex } from "@chakra-ui/core";

const ReviewStars = ({ score, size, reviewCount }) => {
  let stars = [];
  for (let step = 0; step < 5; step++) {
    if (step < score) {
      stars.push("star");
    } else {
      stars.push("not-star");
    }
  }
  return (
    <Flex alignItems="center" mx="auto" display={reviewCount ? "flex" : "block"}>
      {stars.map(
        (item, index) =>
          item === "star"
            ? <Icon
                key={index}
                name="star"
                color="orange.300"
                mx="2px"
                size={size || "14px"}
              />
            : <Icon
                key={index}
                name="star"
                color="gray.300"
                mx="2px"
                size={size || "14px"}
              />
      )}
      {reviewCount
        ? <Text ml="2" color="blue.600">
            {reviewCount}
          </Text>
        : null}
    </Flex>
  );
};

export default ReviewStars;
