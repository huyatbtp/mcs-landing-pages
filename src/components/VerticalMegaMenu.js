import { useState } from "react";
import { Box, Text, Flex, Icon, PseudoBox } from "@chakra-ui/core";

// const tree = [
//   {
//     title: "A",
//     children: [
//       {
//         title: "One",
//         nodes: [
//           {
//             title: "One-One",
//             link: "#"
//           }
//         ]
//       },
//       {
//         title: "Two",
//         nodes: [
//           {
//             title: "Two-One",
//             link: "#"
//           }
//         ]
//       }
//     ],
//     bannerSrc: ""
//   },
//   {
//     title: "B",
//     children: [
//       {
//         title: "One",
//         nodes: [
//           {
//             title: "One-One",
//             link: "#"
//           }
//         ]
//       },
//       {
//         title: "Two",
//         nodes: [
//           {
//             title: "Two-One",
//             link: "#"
//           }
//         ]
//       }
//     ],
//     bannerSrc: ""
//   }
// ];

const MenuContent = ({ data }) => {
  if (data) {
    const { children } = data;
    return (
      <Box
        position="absolute"
        left="200px"
        top="0px"
        px={4}
        py={2}
        width="1000px"
        height="400px"
        bg="white"
        borderWidth="1px"
        borderTop="0px"
        borderLeft="0px"
      >
        <Flex justifyContent="space-between">
          {children.map((item, index) =>
            <Box key={index} width={1 / 2}>
              <Text display="inline-block" fontWeight="bold">
                {item.title}
              </Text>
            </Box>
          )}
        </Flex>
      </Box>
    );
  } else return null;
};
const MenuItem = ({ data, setMenuContent, ...props }) =>
  <PseudoBox
    display="flex"
    justifyContent="space-between"
    alignItems="center"
    borderBottom="1px"
    borderColor="gray.100"
    _hover={{
      cursor: "pointer",
      borderColor: "gray.200",
      color: "white",
      bg: "blue.500"
    }}
    {...props}
  >
    <Text p={2}>
      {data.title}
    </Text>
    {/* <Icon name="chevron-right" size="15px" color="gray.600" bg="transparent" /> */}
  </PseudoBox>;

const VerticalMegaMenu = ({ tree }) => {
  const [menuContent, setMenuContent] = useState(null);
  return (
    <Box
      position="relative"
      width="200px"
      height="400px"
      borderColor="gray.100"
      borderWidth="1px"
      borderTop="0px"
    >
      {tree.map((item, index) =>
        <MenuItem
          data={item}
          key={index}
          onMouseEnter={() => {
            setMenuContent(item);
          }}
          onMouseLeave={() => setMenuContent(null)}
        />
      )}
      <MenuContent data={menuContent} />
    </Box>
  );
};

export default VerticalMegaMenu;
