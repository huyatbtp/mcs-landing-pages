import { Stack, Text } from '@chakra-ui/core'

const QuickLinks = ({ links, ...props }) =>
  <Stack {...props}>
    {links.map((item, index) =>
      <a href={item.href} key={index} title={item.title}>
        <Text mr={4}>
          {item.title}
        </Text>
      </a>
    )}
  </Stack>;

// sample links object
// const links = [
//     {
//       title: "Best Sellers",
//       href: ""
//     },
//     {
//       title: "Product of The Month",
//       href: ""
//     },
//   ];
  

export default QuickLinks