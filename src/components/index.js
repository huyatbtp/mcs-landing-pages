import Footer from "./Footer";
import Header from "./Header";
import ContentWrapper from "./ContentWrapper";
import QuickLinks from "./QuickLinks";
import ReviewStars from "./ReviewStars";
import ServiceReviews from "./ServiceReviews";
import ProductCard from "./ProductCard";
import TextWithUnderLine from "./TextWithUnderLine";
import VerticalMegaMenu from "./VerticalMegaMenu";

export {
  Header,
  Footer,
  ContentWrapper,
  QuickLinks,
  ReviewStars,
  ServiceReviews,
  ProductCard,
  TextWithUnderLine,
  VerticalMegaMenu
};
export default {
  Header,
  Footer,
  ContentWrapper,
  QuickLinks,
  ReviewStars,
  ServiceReviews,
  ProductCard,
  TextWithUnderLine,
  VerticalMegaMenu
};
