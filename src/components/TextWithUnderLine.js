import { Text } from "@chakra-ui/core";

const TextWithUnderLine = ({ children, ...props }) =>
  <Text
    {...props}
    style={{
      backgroundImage:
        "url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiIHdpZHRoPSIxMTkiIGhlaWdodD0iNiI+ICA8cGF0aCBkPSJNMTE3LjQzNCAzLjg1M0M1OS4wMjcgNS45MzMgODQuNzg0LTIuNDYgMS41NjYgMy40MzYiIHN0cm9rZT0iI2ZjMCIgc3Ryb2tlLXdpZHRoPSIyIiBmaWxsPSJub25lIiBzdHJva2UtbGluZWNhcD0icm91bmQiLz48L3N2Zz4=)",
      paddingBottom: "0.1rem",
      backgroundPosition: "bottom left",
      backgroundSize: "100% 0.45rem", // width height
      backgroundRepeat: "no-repeat"
    }}
  >
    {children}
  </Text>;

export default TextWithUnderLine
