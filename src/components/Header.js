import {
  Flex,
  Heading,
  Stack,
  Text,
  Box,
  Input,
  InputGroup,
  InputLeftElement,
  Icon,
  Avatar
} from "@chakra-ui/core";
import { ContentWrapper, QuickLinks } from "./";
const menu = ["Comparisons", "Savings", "Solutions"];
const links = [
  {
    title: "Antivirus",
    href: ""
  },
  {
    title: "Exchange",
    href: ""
  },
  {
    title: "Hardware",
    href: ""
  },
  {
    title: "Office",
    href: ""
  },
  {
    title: "Other Software",
    href: ""
  },
  {
    title: "Project",
    href: ""
  },
  {
    title: "Sharepoint",
    href: ""
  },
  {
    title: "SQL Server",
    href: ""
  },
  {
    title: "Virtualization",
    href: ""
  },
  {
    title: "Visio",
    href: ""
  },
  {
    title: "Windows",
    href: ""
  },
  {
    title: "Windows Server",
    href: ""
  },
  {
    title: "Others",
    href: ""
  }
];
const Header = props =>
  <Box>
    <Flex bg="blue.600" color="white" p="3">
      <Stack isInline spacing={8} align="center">
        {menu.map((item, index) =>
          <Text key={index}>
            {item}
          </Text>
        )}
      </Stack>
    </Flex>
    <ContentWrapper>
      <Flex
        py={8}
        alignItems="center"
        borderBottom="1px"
        borderColor="gray.100"
      >
        <Flex width={2/3}>
          <Heading
            borderBottom="4px"
            borderColor="blue.600"
            mr={8}
            as="h1"
            size="lg"
          >
            MYCHOICESERVICES
          </Heading>
          <Box width={2/3}>
            <InputGroup>
              <InputLeftElement
                children={<Icon name="search" color="gray.300" />}
              />
              <Input type="phone" placeholder="Search" />
            </InputGroup>
          </Box>
        </Flex>
        <Flex>
          <Flex borderRight="1px" pr={4} borderColor="gray.300">
            <Flex alignItems="center" ml={4}>
              <Icon name="user" size="24px" mr={2} />
              <Text> Sign In</Text>
            </Flex>
            <Flex alignItems="center" ml={4}>
              <Icon name="cart" size="24px" mr={2} />
              <Text> Cart</Text>
            </Flex>
          </Flex>
          <Flex ml={4} alignItems="center">
            <Box mr={2}>
              <Avatar
                name="Support"
                src="https://images-na.ssl-images-amazon.com/images/M/MV5BOTU2MTI0NTIyNV5BMl5BanBnXkFtZTcwMTA4Nzc3OA@@._V1_UX172_CR0,0,172,256_AL_.jpg"
              />
            </Box>
            <Box>
              <Text fontWeight="bold">Need Helps?</Text>
              <Text fontSize="sm">Call 800-318-1439</Text>
              <Text fontSize="sm">or Text 949-612-2053</Text>
            </Box>
          </Flex>
        </Flex>
      </Flex>
    </ContentWrapper>
  </Box>;

export default Header;
