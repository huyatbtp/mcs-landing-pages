import { Box, Image, Flex, Text, PseudoBox } from "@chakra-ui/core";
import { ReviewStars } from './'

const CartButton = ({ children, ...props }) =>
  <PseudoBox
    as="button"
    width="100%"
    lineHeight="1.2"
    transition="all 0.2s cubic-bezier(.08,.52,.52,1)"
    border="1px"
    p={2}
    rounded="2px"
    fontSize="14px"
    fontWeight="semibold"
    bg="yellow.400"
    borderColor="yellow.600"
    color="yellow.900"
    _hover={{ bg: "yellow.300" }}
    _active={{
      bg: "#dddfe2",
      transform: "scale(0.98)",
      borderColor: "#bec3c9"
    }}
    _focus={{
      boxShadow:
        "0 0 1px 2px rgba(88, 144, 255, .75), 0 1px 1px rgba(0, 0, 0, .15)"
    }}
    {...props}
  >
    {children}
  </PseudoBox>;


const ProductCard = ({ product, bestseller }) =>
  <Box px={4} py={8} borderWidth="1px" borderLeft="0px" borderBottom="0px">
    {bestseller
      ? <Text
          mb="2"
          bg="orange.100"
          color="orange.900"
          display="inline-block"
          px={1}
          fontSize="sm"
          fontWeight="500"
          borderRadius="2px"
        >
          Best Sellers
        </Text>
      : null}
    <Image src={product.photo} mx="auto" />
    <Box my={4}>
      <Text lineHeight="normal" color="blue.600" fontSize="sm" fontWeight="500">
        {product.title}
      </Text>
      <Text fontSize="xs" fontWeight="bold">
        {product.sku}
      </Text>
    </Box>
    <Box>
      <Text fontWeight="bold" color="red.600">
        {product.price}
      </Text>
      <Flex>
        <Text as="s" mr="2">
          {product.priceCompare}
        </Text>
        <Text>
          {" "}(Save {product.priceSave})
        </Text>
      </Flex>
      <ReviewStars score={4} reviewCount={124}/>
    </Box>
    <CartButton mt={6}>Add To Cart</CartButton>
  </Box>;

export default ProductCard;
