import { Box, Text, Image } from "@chakra-ui/core";
import { ReviewStars } from "./";
const ServiceReviews = ({ score, scale, serviceTitle, serviceImgSrc, bg }) =>
  <Box textAlign="center" width="150px" bg={bg} p="4">
    <Image
      src={serviceImgSrc}
      title={serviceTitle}
      width="100px"
      mx="auto"
      mb="2"
    />
    <ReviewStars score={5} />
    <Text fontWeight="bold" fontSize="lg" my="2" color="green.500">{score}</Text>
    <Text fontWeight="bold" fontSize="xs" color="gray.900">OUT OF {scale || 5}</Text>
  </Box>;

export default ServiceReviews;
