import React from "react";
import Link from "next/link";
import {
  Heading,
  Box,
  Text,
  Flex,
  Button,
  Image,
  PseudoBox
} from "@chakra-ui/core";
import { Header, Footer, QuickLinks, ContentWrapper } from '../src/components'

const links = [
  {
    title: "Best Sellers",
    href: ""
  },
  {
    title: "Product of The Month",
    href: ""
  },
  {
    title: "Monthly Deals",
    href: ""
  },
  {
    title: "Coupons",
    href: ""
  }
];

const CartButton = ({ children, ...props }) =>
  <PseudoBox
    as="button"
    width="100%"
    lineHeight="1.2"
    transition="all 0.2s cubic-bezier(.08,.52,.52,1)"
    border="1px"
    p={2}
    rounded="2px"
    fontSize="14px"
    fontWeight="semibold"
    bg="yellow.400"
    borderColor="yellow.600"
    color="yellow.900"
    _hover={{ bg: "yellow.300" }}
    _active={{
      bg: "#dddfe2",
      transform: "scale(0.98)",
      borderColor: "#bec3c9"
    }}
    _focus={{
      boxShadow:
        "0 0 1px 2px rgba(88, 144, 255, .75), 0 1px 1px rgba(0, 0, 0, .15)"
    }}
    {...props}
  >
    {children}
  </PseudoBox>;


const Banner = props => {
  return (
    <Flex bg="teal.50" color="cyan.800" height="300px">
      <Box lineHeight="tall" width={1 / 3} p={8}>
        <Heading as="h2" mb={4}>
          Upgrade & Save
        </Heading>
        <Text width={3 / 4}>
          Up to 50% off when you upgrade your software to the new version
        </Text>

        <Button
          mt={8}
          rightIcon="arrow-forward"
          variantColor="white"
          variant="outline"
        >
          Shop Now
        </Button>
      </Box>
      <Image
        width={2 / 3}
        objectFit="cover"
        src="https://img-prod-cms-rt-microsoft-com.akamaized.net/cms/api/am/imageFileData/RE3cAQL?ver=0cd1&q=90&h=675&w=830&b=%23FFFFFFFF&aim=true"
      />
    </Flex>
  );
};

const product = {
  title: "Microsoft Project Standard 2019 - License - Download",
  sku: "MFR # 076-05785",
  price: "$319.99",
  priceCompare: "$389.99",
  priceSave: "18%",
  photo:
    "https://cdn.shopify.com/s/files/1/0855/1446/products/MS-Project-Standard-2019-Win-V2_84296d86-6a5d-4d8f-af55-1c638bdc28b2_240x240.png?v=1543449212"
};

const ProductCard = ({ product, bestseller }) =>
  <Box px={4} py={8} borderWidth="1px" borderLeft="0px" borderBottom="0px">
    {bestseller
      ? <Text
          mb="2"
          bg="orange.100"
          color="orange.900"
          display="inline-block"
          px={1}
          fontSize="sm"
          fontWeight="500"
          borderRadius="2px"
        >
          Best Sellers
        </Text>
      : null}
    <Image src={product.photo} />
    <Box my={4}>
      <Text lineHeight="normal" color="blue.600" fontSize="sm" fontWeight="500">
        {product.title}
      </Text>
      <Text fontSize="xs" fontWeight="bold">
        {product.sku}
      </Text>
    </Box>
    <Box>
      <Text fontWeight="bold" color="red.600">
        {product.price}
      </Text>
      <Flex>
        <Text as="s" mr="2">
          {product.priceCompare}
        </Text>
        <Text>
          {" "}(Save {product.priceSave})
        </Text>
      </Flex>
    </Box>
    <CartButton mt={6}>Add To Cart</CartButton>
  </Box>;


const Index = props =>
  <Box>
    <Header />
    <ContentWrapper>
      <QuickLinks isInline py={2} links={links} />
      <Banner />
      <Flex
        mt={8}
        borderTop="0px"
        borderLeft="0px"
        borderRight="0px"
        borderWidth="1px"
      >
        <Box width={1 / 5} borderWidth="1px" borderBottom="0px">
          Filter
        </Box>
        <Box width={4 / 5}>
          <Flex>
            {links.map((item, index) =>
              <ProductCard bestseller key={index} product={product} />
            )}
          </Flex>
          <Flex>
            {links.map((item, index) =>
              <ProductCard key={index} product={product} />
            )}
          </Flex>
        </Box>
      </Flex>
    </ContentWrapper>
    <Footer />
  </Box>;

export default Index;
