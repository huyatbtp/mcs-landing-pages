import React from "react";
import css from "@styled-system/css";
import { Heading, Box, Text, Flex, Button, Image } from "@chakra-ui/core";
import {
  Header,
  Footer,
  ServiceReviews,
  ContentWrapper,
  ProductCard,
  TextWithUnderLine,
  VerticalMegaMenu
} from "../src/components";

const tree = [
    {
      title: "Antivirus",
      children: [
          {
              title: "A-One",
              nodes: [
                  {
                      title: "One-One",
                      link: "#"
                  }
              ]
          },
          {
            title: "A-Two",
            nodes: [
                {
                    title: "Two-One",
                    link: "#"
                }
            ]
        }
      ],
      bannerSrc: ""
    },
    {
      title: "Exchange",
      children: [
          {
              title: "B-One",
              nodes: [
                  {
                      title: "One-One",
                      link: "#"
                  }
              ]
          },
          {
            title: "B-Two",
            nodes: [
                {
                    title: "Two-One",
                    link: "#"
                }
            ]
        }
      ],
      bannerSrc: ""
    },
    {
      title: "Hardware",
      children: [
          {
              title: "A-One",
              nodes: [
                  {
                      title: "One-One",
                      link: "#"
                  }
              ]
          },
          {
            title: "A-Two",
            nodes: [
                {
                    title: "Two-One",
                    link: "#"
                }
            ]
        }
      ],
      bannerSrc: ""
    },
    {
      title: "Office",
      children: [
          {
              title: "A-One",
              nodes: [
                  {
                      title: "One-One",
                      link: "#"
                  }
              ]
          },
          {
            title: "A-Two",
            nodes: [
                {
                    title: "Two-One",
                    link: "#"
                }
            ]
        }
      ],
      bannerSrc: ""
    },
    {
      title: "Project",
      children: [
          {
              title: "A-One",
              nodes: [
                  {
                      title: "One-One",
                      link: "#"
                  }
              ]
          },
          {
            title: "A-Two",
            nodes: [
                {
                    title: "Two-One",
                    link: "#"
                }
            ]
        }
      ],
      bannerSrc: ""
    },
    {
      title: "Sharepoint",
      children: [
          {
              title: "A-One",
              nodes: [
                  {
                      title: "One-One",
                      link: "#"
                  }
              ]
          },
          {
            title: "A-Two",
            nodes: [
                {
                    title: "Two-One",
                    link: "#"
                }
            ]
        }
      ],
      bannerSrc: ""
    },
    {
      title: "SQL Server",
      children: [
          {
              title: "A-One",
              nodes: [
                  {
                      title: "One-One",
                      link: "#"
                  }
              ]
          },
          {
            title: "A-Two",
            nodes: [
                {
                    title: "Two-One",
                    link: "#"
                }
            ]
        }
      ],
      bannerSrc: ""
    },
    {
      title: "Visio",
      children: [
          {
              title: "A-One",
              nodes: [
                  {
                      title: "One-One",
                      link: "#"
                  }
              ]
          },
          {
            title: "A-Two",
            nodes: [
                {
                    title: "Two-One",
                    link: "#"
                }
            ]
        }
      ],
      bannerSrc: ""
    },
    {
      title: "Virtualization",
      children: [
          {
              title: "A-One",
              nodes: [
                  {
                      title: "One-One",
                      link: "#"
                  }
              ]
          },
          {
            title: "A-Two",
            nodes: [
                {
                    title: "Two-One",
                    link: "#"
                }
            ]
        }
      ],
      bannerSrc: ""
    }
  ];

const links = [
  {
    title: "Antivirus",
    href: ""
  },
  {
    title: "Exchange",
    href: ""
  },
  {
    title: "Hardware",
    href: ""
  },
  {
    title: "Office",
    href: ""
  }
];

const Banner = props => {
  return (
    <Flex bg="black" color="white" height="300px">
      <Box lineHeight="tall" width={1 / 3} p={8}>
        <Heading as="h2" mb={4}>
          Save big this time
        </Heading>
        <Text width={3 / 4}>
          Up to 50% off when you upgrade your software to the new version
        </Text>

        <Button
          mt={8}
          rightIcon="arrow-forward"
          variantColor="white"
          variant="outline"
        >
          Shop Now
        </Button>
      </Box>
      <Image
        width={2 / 3}
        objectFit="cover"
        src="http://cdn.mos.cms.futurecdn.net/JhEGmTKH8ZswTwYcfivEYK.jpg"
      />
    </Flex>
  );
};

const Banner2 = props => {
  return (
    <Flex bg="purple.600" color="white" height="300px">
      <Box lineHeight="tall" width={1 / 3} p={8}>
        <Heading as="h2" mb={4}>
          Shop Windows 10
        </Heading>
        <Text width={3 / 4}>
          New features. Updated security. Windows 10 keeps getting better all
          the time.
        </Text>

        <Button
          mt={8}
          rightIcon="arrow-forward"
          variantColor="white"
          variant="outline"
        >
          Shop Now
        </Button>
      </Box>
      <Image
        width={2 / 3}
        objectFit="cover"
        src="https://c.s-microsoft.com/en-us/CMSImages/windows10-laptop.png?version=2eb124e2-b087-ba9a-1b12-bc7fdfa4e15d"
      />
    </Flex>
  );
};

const AboutMCS = props =>
  <Box width={3 / 4} mx="auto" my={16}>
    <Heading as="h2" textAlign="center" lineHeight="base" fontSize="5xl">
      <TextWithUnderLine display="inline-block" p="3">
        Microsoft Volume Licensing
      </TextWithUnderLine>{" "}
      at{" "}
      <TextWithUnderLine display="inline-block">
        Great Prices
      </TextWithUnderLine>{" "}
      with{" "}
      <TextWithUnderLine display="inline-block">
        Professional Support
      </TextWithUnderLine>
    </Heading>
  </Box>;

const product = {
  title: "Microsoft Project Standard 2019 - License - Download",
  sku: "MFR # 076-05785",
  price: "$319.99",
  priceCompare: "$389.99",
  priceSave: "18%",
  photo:
    "https://cdn.shopify.com/s/files/1/0855/1446/products/MS-Project-Standard-2019-Win-V2_84296d86-6a5d-4d8f-af55-1c638bdc28b2_240x240.png?v=1543449212"
};

const Index = props =>
  <Box>
    <Header />
    <ContentWrapper>
      <Flex>
        <VerticalMegaMenu tree={tree}/>
        <AboutMCS />
      </Flex>
      <Box my={16}>
        <Heading as="h3" my={3} fontSize="2xl" fontWeight="500">
          Our best offer for you
        </Heading>
        <Flex borderWidth="1px" borderTop="0px" borderRight="0px">
          {links.map((item, index) =>
            <ProductCard key={index} product={product} />
          )}
        </Flex>
      </Box>
      <Banner />
      <Box my={16}>
        <Heading as="h3" my={3} fontSize="2xl" fontWeight="500">
          Best sellers
        </Heading>
        <Flex borderWidth="1px" borderTop="0px" borderRight="0px">
          {links.map((item, index) =>
            <ProductCard key={index} product={product} />
          )}
        </Flex>
      </Box>
      <Banner2 />
      <Box my={16}>
        <Heading as="h3" my={3} fontSize="2xl" fontWeight="500">
          Trending this week
        </Heading>
        <Flex borderWidth="1px" borderTop="0px" borderRight="0px">
          {links.map((item, index) =>
            <ProductCard key={index} product={product} />
          )}
        </Flex>
      </Box>
      <Box my={16}>
        <Heading as="h3" my={3} fontSize="2xl" fontWeight="500">
          You might also like
        </Heading>
        <Flex borderWidth="1px" borderTop="0px" borderRight="0px">
          {links.map((item, index) =>
            <ProductCard key={index} product={product} />
          )}
        </Flex>
      </Box>
      <TextWithUnderLine
        as="h4"
        textAlign="center"
        fontSize="5xl"
        fontWeight="bold"
        my="16"
        display="inline-block"
        mx="auto"
        pb="3"
      >
        You are at the right place with the right people
      </TextWithUnderLine>
      <Flex alignItems="center" mb="32">
        <Box width={1 / 3}>
          <Text mb={2}>
            <strong>My Choice Software</strong> is a trusted seller of a full
            line of name brand software including certified Microsoft software:
            MS Office, MS Project, MS Windows, Windows Server, and many more.
          </Text>
          <Text mb={2}>
            Our U.S. based support team can help you choose the right version of
            the software you need with a low price and high satisfaction.
          </Text>
          <Text>
            Better yet, our installation services are available via phone,
            email, text, and webchat at no additional cost. Small Businesses
            (SME): come shop with us to enjoy the peace of mind with our
            tailored solutions and the benefits Microsoft volume licensing
            offers.
          </Text>
        </Box>
        <Box
          width={2 / 3}
          alignItems="flex-end"
          mx="auto"
          justifyContent="space-between"
          flexWrap="wrap"
        >
          <Flex justifyContent="center" alignItems="flex-end">
            <ServiceReviews
              score={4.7}
              serviceTitle="Google"
              serviceImgSrc="https://cdn.shopify.com/s/files/1/0855/1446/t/180/assets/google_logo.png?4753001"
            />
            <ServiceReviews
              score="9.77"
              scale="10"
              serviceTitle="ResellerRatings"
              serviceImgSrc="https://cdn.shopify.com/s/files/1/0855/1446/t/180/assets/rr_logo.png?4753001"
            />
            <ServiceReviews
              score="9.2"
              scale="10"
              serviceTitle="TrustPilot"
              serviceImgSrc="https://cdn.shopify.com/s/files/1/0855/1446/t/180/assets/trustpilot-logo.gif?4753001"
            />
          </Flex>
          <Flex mx="auto" justifyContent="center" alignItems="flex-end" mt={8}>
            <ServiceReviews
              score="96%"
              scale="100%"
              serviceTitle="SiteJabber"
              serviceImgSrc="https://cdn.shopify.com/s/files/1/0855/1446/t/180/assets/site_jabber_logo.png?4753001"
            />
            <ServiceReviews
              score={4.7}
              serviceTitle="Facebook"
              serviceImgSrc="https://cdn.shopify.com/s/files/1/0855/1446/t/180/assets/facebook_logo.png?4753001"
            />
          </Flex>
        </Box>
      </Flex>
    </ContentWrapper>
    <Footer />
  </Box>;

export default Index;
